from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'idlib.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', include('blog.urls')),
    url(r'^c/', include('blog.create_urls')),
    url(r'^r/', include('blog.read_urls')),
    url(r'^u/', include('blog.update_urls')),
    url(r'^d/', include('blog.delete_urls')),
]
