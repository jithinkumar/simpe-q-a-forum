from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth import authenticate, login
from django.http import HttpResponse
from django.http import Http404
from django.views.generic.edit import FormView
from .forms import PostForm
from .models import Entry, Blog


def blog(request, username, blog_title, blog_id):
    #blog = Blog.objects.get(pk=blog_id)
    try:
        entry = Entry.objects.get(blog_id=blog_id)
        context = {'entry': entry}
        return render(request, 'show_blog.html',context)
    except Entry.DoesNotExist:
        raise Http404("No Blog here")
    #return HttpResponse("<h1>%s </h1><br><br> %s" % (entry.blog.title,entry.body_text))

def profile(request, username):
    return HttpResponse("Profile of %s" % username)
def mystats(request):
    return HttpResponse("My stats")
def myarticles(request):
    return HttpResponse("myarticle")
def top_list(request):
    return HttpResponse("top_list")
def categories(request,language):
    return HttpResponse("Categories in %s" % language)
def category_blogs(request, language, category):
    return HttpResponse("top_blogs in %s category %s" % (language, category))
def profile_pic(request,username):
    return HttpResponse("profile_pic of %s" % username)
def read_count(request, blog_id):
    return HttpResponse("read count for blog %s" % blog_id )
def likes(request, blog_id):
    return HttpResponse("likes for %s" % blog_id)
def editor(request):
    return render(request, 'index.html')
def sign_in(request):
    if request.method == 'POST':
        username = request.POST['username']
        print(username)
        password = request.POST['password']
        print(password)
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponse("Signin Success")
            else:
                return HttpResponse("Disabled account")
        else:
            return HttpResponse("Sigin failed")
    else:
        return render(request, 'signin.html')

def sign_up_page(request):
    return render(request, 'signup.html')
def rules(request, jspath):
    return redirect('/static/rules/'+jspath)

class EditorView(FormView):
    template_name = 'editor.html'
    form_class = PostForm
    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
       return super(EditorView, self).form_valid(form)
def image_upload(request):
        return HttpResponse("image uploaded")




# Create your views here.
