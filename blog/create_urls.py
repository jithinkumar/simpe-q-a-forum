from django.conf.urls import url

from . import create_views

urlpatterns = [
        url(r'^me/profile/$', create_views.profile, name='create_profile'),
        url(r'^me/blog/publish/$', create_views.publish, name='publish_blog'),
        url(r'^me/image/blog/upload/$', create_views.image_upload, name='imgage_upload'),
        url(r'^me/create-user/$', create_views.create_user, name='create_user'),
        ]
