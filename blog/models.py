from django.db import models
from django.contrib.auth.models import User

class Blog(models.Model):
    title = models.CharField(max_length=255)
    tagline = models.CharField(max_length=255)
    user = models.ForeignKey(User)
    class Meta:
        db_table = 'blog'

class Category(models.Model):
    category_name = models.CharField(max_length=255)
    language      = models.CharField(max_length=255)

class Entry(models.Model):
    blog = models.ForeignKey(Blog)
    sub_title = models.CharField(max_length=255)
    body_text = models.TextField()
    pub_date = models.DateField()
    mod_date = models.DateField()
    authors = models.ManyToManyField(User)
    category = models.ForeignKey(Category)
    image = models.ImageField(upload_to = 'article_images/', null=True, blank=True,max_length=1024)

class Comment(models.Model):
    blog = models.ForeignKey(Blog)
    comment = models.CharField(max_length=255)
    user = models.ForeignKey(User)


