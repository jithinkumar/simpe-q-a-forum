from django.conf.urls import url

from . import delete_views

urlpatterns = [
        url(r'^me/profile/$', delete_views.profile, name='delete_profile'),
        url(r'^me/blog/$', delete_views.delete_blog, name='delete_blog'),
        ]
