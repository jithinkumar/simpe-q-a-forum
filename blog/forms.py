from django.forms import ModelForm,ImageField,Form
from .models import Blog,Entry

class PostForm(ModelForm):
    class Meta:
        model = Entry
        fields = ['sub_title','body_text']

class BlogForm(ModelForm):
    class Meta:
        model = Blog
        fields = ['title']
class ImageUploadForm(Form):
        """Image upload form."""
        image = ImageField()
