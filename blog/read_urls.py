#URLconf file
from django.conf.urls import url

from . import read_views

urlpatterns = [
        url(r'^(?P<username>\w+)/(?P<blog_title>[\w|\W]+)-(?P<blog_id>[0-9]+)/blog/$', read_views.blog, name='blog'),
        url(r'^user/(?P<username>\w+)/$', read_views.profile, name='profile'),
        url(r'^signin/$', read_views.sign_in, name='sign_in'),
        url(r'^signup/$', read_views.sign_up_page, name='sign_up'),
        url(r'^blog/editor/$', read_views.editor, name='editor'),
        url(r'^blog/rules/(?P<jspath>\w+.*js)$', read_views.rules, name='rules'),
        url(r'^my/stats/$', read_views.mystats, name='mystats'),
        url(r'^my/articles/$', read_views.myarticles, name='myarticle'),
        url(r'^top-100/$', read_views.top_list, name='top_list'),
        url(r'^(?P<language>\w+)/categories/$', read_views.categories, name='categories'),
        url(r'^(?P<language>\w+)/(?P<category>\w+)/blogs/$', read_views.category_blogs, name='category_blogs'),
        url(r'^profle-pic/(?P<username>\w+)-.png$', read_views.profile_pic, name='profile_pic'),
        url(r'^(?P<blog_id>[0-9]+)/rcount$', read_views.read_count, name='read_count'),
        url(r'^(?P<blog_id>[0-9]+)/likes$', read_views.likes, name='likes'),
        url(r'^me/image/blog/upload/$', read_views.image_upload, name='imgage_upload')
        ]
