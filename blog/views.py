from django.shortcuts import render
from django.http import HttpResponse
from .models import Blog

def home(request):
    blog_list = Blog.objects.all()
    context = {'blog_list':blog_list}
    return render(request, 'home.html', context)


# Create your views here.
