from django.shortcuts import render
from django.utils import timezone
from django.http import HttpResponse
from django.contrib.auth.models import User
from .forms import PostForm, BlogForm, ImageUploadForm

def profile(request):
    u_uname = "";
    u_email = "";
    u_password = "";
    if request.method == 'POST':
        u_uname = request.POST['user_name']
        u_email = request.POST['email']
        u_password = request.POST['password']
        user = User.objects.create_user(u_uname, u_email, u_password)
        user.save()
        return HttpResponse("created account")
    else:
        return HttpResponse("Try again")
def publish(request):
    b_title = "";
    b_body_text = "";
    if request.method == 'POST':
        #get Entry fields from POST data 
        p = PostForm(request.POST)
        #get Blog title from POST data
        b = BlogForm(request.POST)

        #create Blog model object but don't store into database
        blog = b.save(commit=False)
        #save user_id
        blog.user_id = 1
        #save to database
        blog.save()
        #create Entry model object but don't store into database
        post = p.save(commit=False)
        #save blog id
        post.blog_id = blog.id
        post.pub_date = timezone.now()
        post.mod_date = timezone.now()
        post.category_id = 1
        #save to database
        post.save()
        b_title = request.POST['title']
        b_body_text = request.POST['body_text']
    return HttpResponse("blog published<br><br>%s<br><br>%s"%(b_title,b_body_text))

def image_upload(request):
    #img = ImageUploadForm(request.POST, request.FILES)
    return HttpResponse("image uploaded")
def create_user(request):
    return HttpResponse("user_created")

