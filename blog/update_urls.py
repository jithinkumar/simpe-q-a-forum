from django.conf.urls import url

from . import update_views

urlpatterns = [
        url(r'^me/profile/$', update_views.profile, name='update_profile'),
        ]
